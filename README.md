# smp-patches

Patches to enable SMP on the Espresso processor found in the Wii U.

## Context

The Wii U uses a tri-core PowerPC 750, but there was an errata affecting the load-exclusive and store-exclusive instructions used to implement atomics. To work around it, a cache flush needs to be inserted before each stwcx instruction - on Linux, this means patching compilers, libraries, the kernel, etc.

## Using (Gentoo)

I'm pretty new to Gentoo crossdev, so not 100% sure on this, but here's what I tried.

Copy the patches into the appropriate folders:
```
/etc/portage/patches/cross-powerpc-espresso-linux-gnu/gcc/gcc-14.2.1-espresso-cpu.patch
/etc/portage/patches/cross-powerpc-espresso-linux-gnu/glibc/glibc-2.38-espresso.patch
```

Then bootstrap your crossdev:
```shell
crossdev --genv 'EXTRA_ECONF="--with-cpu=espresso"' --target powerpc-espresso-linux-gnu --libc 2.40 --gcc 14.2.1_p20241221
```

I'm not sure `--with-cpu` is the most Gentoo-y way to do this, need to check with the crossdev folks. Fixing the gcc patch version shouldn't also be needed, but any vaguer and I ended up with 13.3.9999 which the patch doesn't apply to.

Copy the patches again:
```
/usr/powerpc-espresso-linux-gnu/etc/portage/patches/sys-devel/gcc/gcc-14.2.1-espresso-cpu.patch
/usr/powerpc-espresso-linux-gnu/etc/portage/patches/sys-libs/glibc/glibc-2.38-espresso.patch
```

Then we need to make sure any gcc we build also has the right cpu flags:
```
# /usr/powerpc-espresso-linux-gnu/etc/portage/env/espresso.conf
EXTRA_ECONF="--with-cpu=espresso"

# /usr/powerpc-espresso-linux-gnu/etc/portage/package.env
sys-devel/gcc espresso.conf
```

Prevent anything too new from being installed (the patches won't apply cleanly):
```
# /usr/powerpc-espresso-linux-gnu/etc/portage/package.mask
>=sys-libs/glibc-2.41
>=sys-libs/gcc-14.3.9999
```

Crossdev now works as usual.

## Using (crosstool-ng)

Copy patches:
```
crosstool-ng-1.26.0/packages/gcc/13.2.0/gcc-13.2.0-espresso-cpu.patch
crosstool-ng-1.26.0/packages/glibc/2.38/glibc-2.38-espresso.patch
```

YMMV with other versions.

linux-wiiu appropriate config:
```
CT_CONFIG_VERSION="4"
CT_ARCH_POWERPC=y
CT_ARCH_CPU="espresso"
CT_TARGET_VENDOR="espresso"
CT_KERNEL_LINUX=y
CT_GLIBC_KERNEL_VERSION_CHOSEN=y
CT_GLIBC_MIN_KERNEL_VERSION="4.19.0"
```

## Testing

The basic idea is that any stwcx should have a dcbst before it. So we can come up with a horrible test command:

```shell
powerpc-espresso-linux-gnu-objdump -d lib/libc.so.6 | grep -B1 stwcx | grep -v 'stwcx\|dcbst\|--'
```

This will disassemble libc.so, look for stwcx and print the instruction that came before it, then filter out expected instructions (stwcx, dcbst) and the -- seperator. So this prints "instructions that came before stwcx that aren't dcbst".

To investigate further, remove the last grep and maybe increase the context (I like `-B4`). There are sometimes some false positives where you get `dcbst, or, stwcx` or the like.

We can turn this into a `find` command to scan an entire system:

```shell
find lib/ -executable -print -exec sh -c "powerpc-espresso-linux-gnu-objdump -d {} | grep -B1 stwcx | grep -v 'stwcx\|dcbst\|--'" \; 2>/dev/null
```

It will print the filenames as it scans, that's normal.
